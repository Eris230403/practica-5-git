package mx.unitec.moviles.practica5.helper
//metodo implementado para realizar que directamente el almacenamiento es directo o externo
//esto casi no se aplica viene por default que te pregunte donde guardarlo
import android.os.Environment

class ESHelper {
    companion object {
        fun IsWritable(): Boolean{
            return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
        }

        fun IsReabable(): Boolean{
            return Environment.getExternalStorageState() in
                    setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
        }
    }
}